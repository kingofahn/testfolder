package com.gms.web.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.gms.web.domain.InsaComDTO;
import com.gms.web.domain.MemberDTO;
import com.gms.web.service.MemberService;

@Controller
@SessionAttributes("ctx")
public class HomeController {
	@Autowired MemberService memberService;
	@Autowired InsaComDTO insaCom;
	@Autowired List<HashMap<?, ?>> lst;
	static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session, HttpServletRequest request) {
		String  ctx = request.getContextPath();
		logger.info("\n --------- Welcome {} !! ----------","Home");
		session.setAttribute("ctx", ctx);
		return "public:common/content.tiles";
	}
	@RequestMapping("/move/{prefix}/{dir}/{page}")
	public String move(
			@PathVariable String prefix,
			@PathVariable String dir,
			@PathVariable String page
			,Model model) {
		logger.info("\n --------- HomeController {} !!--------","move()");
		String path = prefix+":"+dir+"/"+page+".tiles";
		logger.info("\n move page >>> {}",path);
		model.addAttribute("ipsa", memberService.insaCom("ipsa"));
		model.addAttribute("jikwi", memberService.insaCom("jikwi"));
		model.addAttribute("gyegeub", memberService.insaCom("gyegeub"));
		model.addAttribute("email", memberService.insaCom("email"));
		model.addAttribute("booseo", memberService.insaCom("booseo"));
		model.addAttribute("deunggeub", memberService.insaCom("deunggeub"));
		model.addAttribute("kosa", memberService.insaCom("kosa"));
		model.addAttribute("count", memberService.count()+1);
		return path;
	}
}
