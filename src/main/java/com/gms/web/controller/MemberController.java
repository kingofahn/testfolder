package com.gms.web.controller;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.gms.web.domain.MemberDTO;
import com.gms.web.service.MemberService;
@Controller
@RequestMapping("/member")
@SessionAttributes("user")
public class MemberController {
	static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	@Autowired MemberDTO member;
	@Autowired MemberService memberService;
	@RequestMapping("/add")
	/*@RequestMapping(value="/add", method=RequestMethod.POST)*/
	public String add(@ModelAttribute("member") MemberDTO member , Model model) {
		logger.info("\n --------- MemberController {} !!--------","add");
		System.out.println("member.getSabun : " + member.getSabun());
		member.setCarrier("구현x");
		member.setCmp_reg_image("구현x");
		String today = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
		System.out.println("member.get sabun: " + member.getSabun());
		System.out.println("member.get years: " + member.getYears());
		System.out.println("member.get reg_no: " + member.getReg_no());
		System.out.println("입력된 member : " + member.toString());
		member.setYears(String.valueOf(Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()))-(Integer.parseInt(member.getReg_no().substring(0, 2))+1900-1)));
		System.out.println(member.getReg_no().split("-")[1].substring(0,1));
		if(member.getJoin_day()==null){
			member.setJoin_day(today);
		}
		switch (member.getReg_no().split("-")[1].substring(0,1)) {
			case "1":case "3":
				member.setSex("남");
				break;
			case "2":case "4":
				member.setSex("여");
				break;
			case "5":case "6":
				member.setSex("외국인");
				break;
			default:
				member.setSex("");
				break;
		}
		member.setEmail(member.getEmail1()+member.getEmail2());
		memberService.add(member);
		System.out.println("내용 추가된 member : " + member.toString());
		model.addAttribute("user", memberService.selectOne(member.getSabun()));
		System.out.println("model.toString() : " + model.toString());
		return "public:member/modify.tiles";
	}
	
	@RequestMapping("/list")
	public void list() {}
	
	@RequestMapping("/search")
	public void search() {}
	
	@RequestMapping("/retrieve")
	public void retrieve(@ModelAttribute("member") MemberDTO member, Model model) {
		logger.info("\n --------- MemberController {} !!--------","retrieve");
		model.addAttribute("user",memberService.retrieve(member));
	}
	
	@RequestMapping("/count")
	public void count() {}
	
	@RequestMapping(value="/modify", method=RequestMethod.POST)
	public String modify(@ModelAttribute("member") MemberDTO member, 
						@ModelAttribute("user") MemberDTO user,
						Model model) {
		logger.info("\n --------- MemberController {} !!--------","modify");
		logger.info("user : {}", user);
		logger.info("member : {}", member);
		/*member.setUserid(user.getUserid());*/
		memberService.modify(member);
		model.addAttribute("user", memberService.retrieve(member));
		return "public:member/login.tiles";
	}
	
	@RequestMapping(value="/remove", method=RequestMethod.POST)
	public String remove(@ModelAttribute("member") MemberDTO member,
						@ModelAttribute("user") MemberDTO user,
						Model model) {
		logger.info("\n --------- MemberController {} !!--------","remove");
		/*member.setUserid(user.getUserid());*/
		memberService.remove(member);
		return "redirect:/";
	}
	
	@RequestMapping("/fileUpload")
	public void fileUpload() {} 
}