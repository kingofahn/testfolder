package com.gms.web.mapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.gms.web.domain.InsaComDTO;
import com.gms.web.domain.MemberDTO;
@Repository
public interface MemberMapper {
	public void insert(MemberDTO p);
	public List<HashMap<?, ?>> insaCom(String p);
	public List<?> selectList(Map<?,?>p) ;
	public List<?> selectSome(Map<?,?>p) ;
	public MemberDTO selectOne(MemberDTO p) ;
	public int count() ;
	public void update(MemberDTO p) ;
	public void delete(MemberDTO p) ;
	public MemberDTO login(MemberDTO p) ;
	public MemberDTO selectOne(String p);
}
