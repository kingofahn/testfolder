package com.gms.web.domain;

import org.springframework.stereotype.Component;
@Component // bean으로 쓴다는 의미
public class MemberDTO {
	private String	gubun,
					sabun,
					years,
					zip,
					salary,
					join_day,
					retire_day,
					put_yn,
					name,
					reg_no,
					eng_name,
					self_intro,
					phone,
					hp,
					carrier,
					pos_gbn_code,
					cmp_reg_no,
					cmp_reg_image,
					sex,
					email1,
					email2,
					email,
					addr1,
					addr2,
					dept_code,
					join_gbn_code,
					id,
					pwd,
					kosa_reg_yn,
					kosa_class_code,
					mil_yn,
					mil_type,
					mil_level,
					mil_startdate,
					mil_enddate,
					job_type,
					gart_level,
					self_Stringro,
					crm_name;

	public String getGubun() {
		return gubun;
	}

	public void setGubun(String gubun) {
		this.gubun = gubun;
	}

	public String getSelf_intro() {
		return self_intro;
	}

	public void setSelf_intro(String self_intro) {
		this.self_intro = self_intro;
	}

	public void setSabun(String sabun) {
		this.sabun = sabun;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public void setJoin_day(String join_day) {
		this.join_day = join_day;
	}

	public void setRetire_day(String retire_day) {
		this.retire_day = retire_day;
	}

	public void setPut_yn(String put_yn) {
		this.put_yn = put_yn;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setReg_no(String reg_no) {
		this.reg_no = reg_no;
	}

	public void setEng_name(String eng_name) {
		this.eng_name = eng_name;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public void setPos_gbn_code(String pos_gbn_code) {
		this.pos_gbn_code = pos_gbn_code;
	}

	public void setCmp_reg_no(String cmp_reg_no) {
		this.cmp_reg_no = cmp_reg_no;
	}

	public void setCmp_reg_image(String cmp_reg_image) {
		this.cmp_reg_image = cmp_reg_image;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public void setDept_code(String dept_code) {
		this.dept_code = dept_code;
	}

	public void setJoin_gbn_code(String join_gbn_code) {
		this.join_gbn_code = join_gbn_code;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public void setKosa_reg_yn(String kosa_reg_yn) {
		this.kosa_reg_yn = kosa_reg_yn;
	}

	public void setKosa_class_code(String kosa_class_code) {
		this.kosa_class_code = kosa_class_code;
	}

	public void setMil_yn(String mil_yn) {
		this.mil_yn = mil_yn;
	}

	public void setMil_type(String mil_type) {
		this.mil_type = mil_type;
	}

	public void setMil_level(String mil_level) {
		this.mil_level = mil_level;
	}

	public void setMil_startdate(String mil_startdate) {
		this.mil_startdate = mil_startdate;
	}

	public void setMil_enddate(String mil_enddate) {
		this.mil_enddate = mil_enddate;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public void setGart_level(String gart_level) {
		this.gart_level = gart_level;
	}

	public void setSelf_Stringro(String self_Stringro) {
		this.self_Stringro = self_Stringro;
	}

	public String getSabun() {
		return sabun;
	}

	public String getJoin_day() {
		return join_day;
	}

	public String getRetire_day() {
		return retire_day;
	}

	public String getPut_yn() {
		return put_yn;
	}

	public String getName() {
		return name;
	}

	public String getReg_no() {
		return reg_no;
	}

	public String getEng_name() {
		return eng_name;
	}

	public String getPhone() {
		return phone;
	}

	public String getHp() {
		return hp;
	}

	public String getCarrier() {
		return carrier;
	}

	public String getPos_gbn_code() {
		return pos_gbn_code;
	}

	public String getCmp_reg_no() {
		return cmp_reg_no;
	}

	public String getCmp_reg_image() {
		return cmp_reg_image;
	}

	public String getSex() {
		return sex;
	}

	public String getYears() {
		return years;
	}

	public String getEmail() {
		return email;
	}

	public String getZip() {
		return zip;
	}

	public String getAddr1() {
		return addr1;
	}

	public String getAddr2() {
		return addr2;
	}

	public String getDept_code() {
		return dept_code;
	}

	public String getJoin_gbn_code() {
		return join_gbn_code;
	}

	public String getId() {
		return id;
	}

	public String getPwd() {
		return pwd;
	}

	public String getSalary() {
		return salary;
	}

	public String getKosa_reg_yn() {
		return kosa_reg_yn;
	}

	public String getKosa_class_code() {
		return kosa_class_code;
	}

	public String getMil_yn() {
		return mil_yn;
	}

	public String getMil_type() {
		return mil_type;
	}

	public String getMil_level() {
		return mil_level;
	}

	public String getMil_startdate() {
		return mil_startdate;
	}

	public String getMil_enddate() {
		return mil_enddate;
	}

	public String getJob_type() {
		return job_type;
	}

	public String getGart_level() {
		return gart_level;
	}

	public String getSelf_Stringro() {
		return self_Stringro;
	}

	public String getCrm_name() {
		return crm_name;
	}

	public void setCrm_name(String crm_name) {
		this.crm_name = crm_name;
	}
	
	@Override
	public String toString() {
		return "MemberDTO [sabun=" + sabun + ", years=" + years + ", zip=" + zip + ", salary=" + salary + ", join_day="
				+ join_day + ", retire_day=" + retire_day + ", put_yn=" + put_yn + ", name=" + name + ", reg_no="
				+ reg_no + ", eng_name=" + eng_name + ", phone=" + phone + ", hp=" + hp + ", carrier=" + carrier
				+ ", pos_gbn_code=" + pos_gbn_code + ", cmp_reg_no=" + cmp_reg_no + ", cmp_reg_image=" + cmp_reg_image
				+ ", sex=" + sex + ", email=" + email + ", addr1=" + addr1 + ", addr2=" + addr2 + ", dept_code="
				+ dept_code + ", join_gbn_code=" + join_gbn_code + ", id=" + id + ", pwd=" + pwd + ", kosa_reg_yn="
				+ kosa_reg_yn + ", kosa_class_code=" + kosa_class_code + ", mil_yn=" + mil_yn + ", mil_type=" + mil_type
				+ ", mil_level=" + mil_level + ", mil_startdate=" + mil_startdate + ", mil_enddate=" + mil_enddate
				+ ", job_type=" + job_type + ", gart_level=" + gart_level + ", self_Stringro=" + self_Stringro
				+ ", crm_name=" + crm_name + "]";
	}
}
