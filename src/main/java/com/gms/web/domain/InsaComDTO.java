package com.gms.web.domain;
import org.springframework.stereotype.Component;
@Component // bean으로 쓴다는 의미
public class InsaComDTO {
	private String gubun, code, name, note;

	@Override
	public String toString() {
		return "InsaComDTO [gubun=" + gubun + ", code=" + code + ", name=" + name + ", note=" + note + ", toString()="
				+ super.toString() + "]";
	}

	public String getGubun() {
		return gubun;
	}

	public void setGubun(String gubun) {
		this.gubun = gubun;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
