package com.gms.web.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gms.web.domain.MemberDTO;

public interface MemberService {
	public void add(MemberDTO p) ;
	public List<?> list(Map<?,?>p) ;
	public List<?> search(Map<?,?>p) ;
	public List<HashMap<?, ?>> insaCom(String p);
	public MemberDTO retrieve(MemberDTO p) ;
	public int count() ;
	public void modify(MemberDTO p) ;
	public void remove(MemberDTO p) ;
	public MemberDTO selectOne(String sabun);
}
