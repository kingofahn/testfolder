"use strict";
var app  = app || {};
var user = user || {}; 
app = {
		init : function(x){
			console.log('step 1')
			app.session.context(x);  // 경로설정
			app.onCreate();          // 생성자 느낌
		},
		onCreate : function(){
			console.log('step 3');
			app.setContentView();
			$('#login_btn').click(function(){
				location.href = app.x()+'/move/public/member/login';
			});
			$('#modify_btn').click(function(){
				location.href = app.x()+'/member/retrieve/'+user.get('userid')+'/modify';
			});
			$('#remove_btn').click(function(){   
				location.href = app.x()+'/member/retrieve/'+user.get('userid')+'/remove';
			});
			$('#joinForm_Btn').click(function(){
                $('#joinForm').attr({action:app.x()+"/member/add", method:"POST"}).submit();
            });
		},
		setContentView : function(){
			console.log('step 4' + app.session.path('ctx'));
		}
};

app.session ={
	context : function(x) {
		console.log('step 2' + x)
		sessionStorage.setItem('ctx',x);
		sessionStorage.setItem('js',x+'/resources/js');
		sessionStorage.setItem('cs',x+'/resources/css');
		sessionStorage.setItem('img',x+'/resources/img');
	},
	path : function(x) {
		return sessionStorage.getItem(x);
	}
};

app.x = function(){
	return app.session.path('ctx');
};
app.j = function(){
	return app.session.path('js');
};
app.c = function(){
	return app.session.path('css');
};
app.i = function(){
	return app.session.path('img');
};	