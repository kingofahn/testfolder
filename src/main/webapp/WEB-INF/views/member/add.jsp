<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<form id = "joinForm" name="joinForm">
			<div class="joinInput">
				<div class="joinInput1">
					<img src="https://www.ankarsrum.com/wp-content/uploads/2018/01/no-image-icon-.png"><br>
					<button type="button" id="photo">사진올리기</button><br>
					
					<div class="text"> 입사구분  </div>
					
					<select name="gubun" id="gubun">
						<c:forEach items="${ipsa}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>
					
					<div class="text">* 군필여부 </div>
					<select name="mil_yn" id="mil_yn"></select><br>	
					
					<div class="text">군별 </div>
					<select name="mil_type" id="mil_type" ></select><br>
					
					<div class="text">계급 </div>
					<select name="mil_level" id="mil_level">
						<c:forEach items="${gyegeub}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>
					<div class="text">입영일자 </div>
					<input type="date" name="mil_startdate" id="mil_startdate"><br>	
					<div class="text">전역일자  </div>
					<input type="date" name="mil_enddate" id="mil_enddate"><br>
				</div>
				
				<div class="joinInput2">
					<div class="text">*사번 </div>
					<input type="text" name="sabun" id="sabun" value="${count}" class="blockInput" readonly/> <br>
					<div class="text">*ID </div>
					<input type="text" name="id" id="id"/> <br>
					<div class="text">전화번호 </div>
					<input type="text" name="phone" /> <br>
					<div class="text">주소 </div>
					<input type="text" name="zip" placeholder="우편번호" /><br> 
					<div class="text" > <p>   </p> </div>
					<input type="text" name="addr1" placeholder="주소"/> <br>
					<div class="text">  <p>   </p> </div>
					<input type="text" name="addr2" placeholder="세부주소"/> <br>
					<div class="text">직위 </div>
					<select name="pos_gbn_code" id="pos_gbn_code">
						<c:forEach items="${jikwi}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>
					<div class="text">등급 </div>
					<select name="gart_level" id="gart_level">
						<c:forEach items="${deunggeub}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>

					
					<div class="text">자기소개 </div>
					<input type="text" name="self_intro" id="self_intro" placeholder="100자 내외로 적으시오"/> <br>
				</div>
				
				<div class="joinInput3">
					<div class="text">*한글성명 </div>
					<input type="text" name="name" id="name"/> <br>
					<div class="text">*패스워드 </div>
					<input type="password" name="pwd" id="pwd"/> <br>
					<div class="text">*패스워드 확인 </div>
					<input type="password" id="passwordCheck"><br>	
					<div class="text">*핸드폰번호 </div>
					<input type="text" name="hp" id="hp"/> <br>
					<div class="text">*이메일 </div>
					<input type="text" name="email1" id="email1"/><br>
					<div class="text"><p>  </p> </div>
					<select name="email2" id="email2">
						<c:forEach items="${email}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>
					<div class="text">부서 </div>
					<select name="dept_code" id="dept_code">
						<c:forEach items="${booseo}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>
					<div class="text">투입여부 </div>
					<select name="put_yn" id="put_yn">
						<option value="none">선택</option>
						<option value="y">y</option>
						<option value="n">n</option>
					</select><br>				
					<div class="text">KOSA 등록 </div>
					<select name="kosa_reg_yn" id="kosa_reg_yn">
						<option value="none"></option>
						<option value="y">Y</option>
						<option value="n" >N</option>
					</select><br>
					
					<div class="text">KOSA등급 </div>
					<select name="kosa_class_code" id="kosa_class_code">
						<c:forEach items="${kosa}" var="list">
							<option value="${list.name}"> ${list.name} </option>
						</c:forEach>
					</select><br>
					<div class="text">업체명 </div>
					<input type="text" name="crm_name" id="crm_name"/> <br>
					
					<div class="text">사업자번호 </div>
					<input type="text" name="cmp_reg_no" id="cmp_reg_no"/> <br>
					
	<!-- 				사업자등록증
					<button type="button" id="cmp_reg_image">미리보기</button>
					<button type="button" id="cmp_reg_image_register">등록</button><br>
					
					이력서
					<button type="button" id="carrier">미리보기</button>
					<button type="button" id="carrier_register">파일업로드</button><br> -->
				</div>
				
				
				<div class="joinInput4">
					<div class="text">영문성명 </div>
					<input type="text" name="eng_name" id="eng_name"><br>	
					<div class="text">*주민번호 </div>
					<input type="text" name="reg_no" id="reg_no"/><br>	
					
					<div class="text">직종체크 </div>
					<select name="job_type" id="job_type">
						<option value="none">선택</option>
						<option value="a">a</option>
						<option value="b">b</option>
					</select><br>		
					<div class="text">연봉(만원) </div>
					<input type="text" name="salary" id="salary" placeholder="만원"><br>	
					
					<div class="text">*입사날짜 </div>
					<input type="date" name="join_day" id="join_day"><br>	
					
					<div class="text">퇴사일자 </div>
					<input type="date" name="retire_day" id="retire_day"><br>	
					<input type=button id="joinForm_Btn" value="등록">
				</div>
			</div>
		</form>
		
<script>
$('#joinForm_Btn').click(function(){
	let mil_yn = $('#mil_yn').val();
	let sabun = $('#sabun').val();
	let id = $('#id').val();
	let reg_no = $('#reg_no').val();
	let pwd = $('#pwd').val();
	let name = $('#name').val();
	let hp = $('#hp').val();
	let email1 = $('#email1').val();
	let email2 = $('#email2').val();
	let join_day = $('#join_day').val();
	let passwordCheck = $('#passwordCheck').val();
	let x = $('#mil_startdate').val();
	let y = $('#mil_enddate').val();
	let mil_startdate = new Date(x.split('-')[0],(x.split('-')[1]),x.split('-')[2]);
	let mil_enddate = new Date(y.split('-')[0],(y.split('-')[1]),y.split('-')[2]);
	let diff = (mil_enddate.getTime()-mil_startdate.getTime()) / (1000*60*60*24);
	if(!mil_yn){
 		$('#mil_yn').addClass('validator');
		alert('군필여부를 입력해주세요');
	}else if(!id){
		alert('이름을 입력해주세요');
	}else if(!name){
		alert('이름을 입력해주세요');
	}else if(!pwd){
		alert('패스워드를 입력해주세요');
	}else if(!passwordCheck){
		alert('패스워드 확인을 입력해주세요');
	}else if(pwd!=passwordCheck){
		alert('입력하신 패스워드와 패스워드 확인이 불일치합니다'); 
	}else if(!hp){
		alert('핸드폰 번호를 입력해주세요');
	}else if(!email1){
		alert('이메일(앞)를 입력해주세요');
	}else if(!email2){
		alert('이메일(뒤)를 입력해주세요');
	}else if(!reg_no){
		alert('주민번호를 입력해주세요');
	}else if (diff<1) {
		alert('전역일자가 입영일자 보다 후에 입력되어야 합니다.');
	}else if(!join_day){
		alert('입사날짜 입력해주세요');
	} else {
		$('#joinForm').attr({action:"${ctx}/member/add", method:"POST"}).submit();
	}
}); 

$('#kosa_reg_yn').attr({onchange:"changeKosa()"});
function changeKosa() {
	var kosaSelect = document.getElementById("kosa_reg_yn");
	var kosaValue = kosaSelect.options[kosaSelect.selectedIndex].value;
	if(kosaValue=="n"){
		$('#kosa_class_code').attr('disabled', true).addClass('blockInput');
		$('#crm_name').attr('disabled', true).addClass('blockInput');
		$('#cmp_reg_no').attr('disabled', true).addClass('blockInput');
	}else{
		$('#kosa_class_code').attr('disabled', false);
		$('#crm_name').attr('disabled', false).removeClass('blockInput');
		$('#cmp_reg_no').attr('disabled', false).removeClass('blockInput');
	}
}

$('#mil_yn').attr({onchange:"changeMil()"}).append(
					$('<option/>').attr({value:"none"}).html(""),
					$('<option/>').attr({value:"군필"}).html("군필"),
					$('<option/>').attr({value:"미필"}).html("미필"),
					$('<option/>').attr({value:"none"}).html("해당없음"));

function changeMil() {
	$('#mil_type').empty();
	var milSelect = document.getElementById("mil_yn");
	var selectValue = milSelect.options[milSelect.selectedIndex].value;
	if(selectValue=="미필" || selectValue=="none"){
		$('#mil_type').attr('disabled', true).addClass('blockInput');
		$('#mil_startdate').attr('disabled', true).addClass('blockInput');
		$('#mil_enddate').attr('disabled', true).addClass('blockInput');
		$('#mil_level').attr('disabled', true).addClass('blockInput');
		$('#mil_type').append(
				$('<option/>').attr({value:"none"}).html(""));
	} else {
		$("#mil_type").attr('disabled', false).removeClass('blockInput');
		$("#mil_startdate").attr('disabled', false).removeClass('blockInput');
		$("#mil_enddate").attr('disabled', false).removeClass('blockInput');
		$("#mil_level").attr('disabled', false).removeClass('blockInput');
		$('#mil_type').append(
				$('<option/>').attr({value:"none"}).html(""),
				$('<option/>').attr({value:"육군"}).html("육군"),
				$('<option/>').attr({value:"해군"}).html("해군"),
				$('<option/>').attr({value:"공군"}).html("공군"),
				$('<option/>').attr({value:"카투사"}).html("카투사"));	
	}
}


/* $('#joinForm_Btn').click(function(){
	alert('클릭');
	let sabun = $('#sabun').val();
	alert('사번 : ' + sabun);
 	let id = $('#id').val();
	let reg_no = $('#reg_no').val();
	let pwd = $('#pwd').val();
	let name = $('#name').val();
	let hp = $('#hp').val();
	let email1 = $('#email1').val();
	let email2 = $('#email2').val();
	let join_day = $('#join_day').val();
	let passwordCheck = $('#passwordCheck').val();
	let x = $('#mil_startdate').val();
	let y = $('#mil_enddate').val();
	let mil_startdate = new Date(x.split('-')[0],(x.split('-')[1]),x.split('-')[2]);
	let mil_enddate = new Date(y.split('-')[0],(y.split('-')[1]),y.split('-')[2]);
	let diff = (mil_enddate.getTime()-mil_startdate.getTime()) / (1000*60*60*24); 
				$.ajax({
					url:"${ctx}/member/add",
					method:'post',
					contentType:'application/json',
					data:JSON.stringify({
						sabun:$('#sabun').val()/* ,
						name:$('#name').val(),
						nickname:$('#nickname').val(),
						password:$('#password').val(),
						birthdate:$('#birthdate').val(),
						phone:$('#phone').val(),
						address:$('#address').val(),
						zipcode:$('#zipcode').val(),
						profileimg:'default.jpg',
						kakao: '1'
					}),
					success:function(d){
						alert('성공적으로 가입되었습니다.');
					}
				}); 
}); */

</script>		






  