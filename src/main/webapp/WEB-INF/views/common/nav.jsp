<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!-- Icons Grid -->
    <div id="nav">
    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
           <div class="col-lg-4" id="home">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-screen-desktop m-auto text-primary"></i>
   			  </div>
            	  <h3>Home</h3>
            </div>
          </div>
        
          <div class="col-lg-4" id="register">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-screen-desktop m-auto text-primary"></i>
   			  </div>
            	  <h3>Input</h3>
            </div>
          </div>
          <div class="col-lg-4" id="search">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-layers m-auto text-primary"></i>
              </div>
	              <h3>Search</h3>
            </div>
          </div>
        </div>
      </div>
    </section>
    </div>
    
    <script>
	$('#register').click(function (){
		location.href = '${ctx}'+'/move/public/member/add';
	});
	$('#search').click(function (){   
		location.href = '${ctx}' +'/move/public/member/add';
	});
	$('#home').click(function (){   
		location.href = '${ctx}' +'/';
	});
    </script>
    	
    
    